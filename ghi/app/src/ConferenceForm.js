import React from "react"

class ConferenceForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            max_presentations: "",
            max_attendees: "",
            locations: [],
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStart = this.handleStart.bind(this)
        this.handleEnd = this.handleEnd.bind(this)
        this.handleDescription = this.handleDescription.bind(this)
        this.handleMaxPresentations = this.handleMaxPresentations.bind(this)
        this.handleMaxAttendees = this.handleMaxAttendees.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)
        this.handleLocationChange=this.handleLocationChange.bind(this)
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location:value})
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name:value})
    }
    handleStart(event) {
        const value = event.target.value
        this.setState({starts:value})
    }
    handleEnd(event) {
        const value = event.target.value
        this.setState({ends:value})
    }
    handleDescription(event) {
        const value = event.target.value
        this.setState({description:value})
    }
    handleMaxPresentations(event) {
        const value = event.target.value
        this.setState({max_presentations:value})
    }
    handleMaxAttendees(event) {
        const value = event.target.value
        this.setState({max_attendees:value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        // triple dot means copy
        const data = {...this.state};
        console.log(data)
        
        delete data.locations;
        console.log(data); 

        const conferenceURL = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                }
            };
        const response = await fetch(conferenceURL, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: ''
        
            }
            this.setState(cleared)
        
        }else{
            console.error("Error Not a good Response")
        }
    }

    async componentDidMount () {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations})
            
        }
      }
    
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" value={this.state.name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb3">
                        <input onChange={this.handleStart} placeholder="Start" required type="date" id="start" name="starts" className="form-control" value={this.state.starts}/>
                        <label htmlFor="start">Start Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleEnd} placeholder="End" required type="date" id="end" name="ends" className="form-control" value={this.state.ends}/>
                        <label htmlFor="end">End Date</label>
                    </div>
                    <div>
                        <label>Description</label>
                        <textarea onChange={this.handleDescription} required name="description" id="description" className="form-control" value={this.state.description} />
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxPresentations} placeholder="Max Presentations" required type="number" id="maxPresentations" name="maxPresentations" className="form-control" value={this.state.max_presentations} />
                        <label htmlFor="maxPresentations">Max Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleMaxAttendees} placeholder="Max Attendees" required type="number" id="maxAttendees" name="maxAttendees" className="form-control" value={this.state.max_attendees} />
                        <label htmlFor="max_attendees">Max Attendees</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select value={this.state.location} onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                            <option value ="" >Choose a location</option>
                            {this.state.locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                
                                {location.name} 
                                
                            </option>
                        );
                    })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
          </div>
        </div>
      );
    }
  }
export default ConferenceForm